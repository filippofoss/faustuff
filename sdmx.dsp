import("stdfaust.lib");
//process = _,_ <: +,- : /(sqrt(2)),/(sqrt(2));
//process = _,_ <: +/(sqrt(2)),-/(sqrt(2)) ;
nsum = +/(sqrt(2));
ndif = -/(sqrt(2));
sdmx = _,_ <: nsum, ndif;
//test per valutare la normalizzazione, priva di perdite e guadagni
//process = os.osc(1000), no.pink_noise : seq(i,16,sdmx);
process = sdmx;
